#include "protocol.h"

namespace hwo_protocol
{

  jsoncons::json make_request(const std::string& msg_type, const jsoncons::json& data)
  {
    jsoncons::json r;
    r["msgType"] = msg_type;
    r["data"] = data;
    return r;
  }

  jsoncons::json make_create(const std::string& name, const std::string& key)
  {
    jsoncons::json data;
    data["botId"]["name"] = name;
    data["botId"]["key"] = key;
    data["trackName"] = "usa";
    data["password"] = "derp";
    data["carCount"] = 3;
    std::cout << "pre create" << std::endl;
    return make_request("createRace", data);
  }

  jsoncons::json make_join(const std::string& name, const std::string& key)
  {
    jsoncons::json data;
    data["name"] = name;
    data["key"] = key;
    return make_request("join", data);
  }

  jsoncons::json make_ping()
  {
    return make_request("ping", jsoncons::null_type());
  }

  jsoncons::json make_throttle(double throttle)
  {
    return make_request("throttle", throttle);
  }

  jsoncons::json make_turbo(const std::string& msg)
  {
    return make_request("turbo", msg);
  }

  jsoncons::json make_lane_switch(const std::string& msg)
  {
    jsoncons::json data;
    data["data"] = msg;
    return make_request("switchLane", msg);
  }

}  // namespace hwo_protocol
