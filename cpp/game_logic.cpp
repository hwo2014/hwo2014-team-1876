#include "game_logic.h"
#include "protocol.h"

using namespace hwo_protocol;

game_logic::game_logic()
  : action_map
    {
      { "join", &game_logic::on_join },
      { "gameStart", &game_logic::on_game_start },
      { "carPositions", &game_logic::on_car_positions },
      { "crash", &game_logic::on_crash },
      { "gameEnd", &game_logic::on_game_end },
      { "error", &game_logic::on_error },
      { "turboAvailable", &game_logic::on_turbo},
      { "yourCar", &game_logic::on_your_car},
      { "gameInit", &game_logic::on_game_init},
      { "spawn", &game_logic::on_spawn}
      //TODO: , gameInit, turboAvailable, lapFinished, finish, spawn, all the others...
      //Also prefer lanes that are empty and switch to them if possible
    }
{
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
    return (action_it->second)(this, data);
  }
  else
  {
    std::cout << "Unknown message type: " << msg_type << std::endl;
    return { make_ping() };
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
  my_car_throttle = max_throttle;
  my_first_corner = false;
  my_turbo_boost = false;
  my_inner_lane = false;
  next_curve_angle = 0.0;
  std::cout << "Joined" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
  std::cout << "Race started" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
  //std::cout << data.as<std::string>().c_str() << std::endl;
  for(int i = 0; i < data.size(); i++)
  {
    if(data[i]["id"]["name"].as<std::string>().compare(my_car_id) == 0)
    {
      //TODO: 
      //add turbo (check if track is straight first), track prioritizer 
      //(switch track to the one that seems most free),
      //write a proper lane selector that scans more rigorously for the next corner

      my_piece_index = data[i]["piecePosition"]["pieceIndex"].as<int>();
      my_start_lane_index = data[i]["piecePosition"]["lane"]["startLaneIndex"].as<int>();
      int my_end_lane_index_old = my_end_lane_index;
      my_end_lane_index = data[i]["piecePosition"]["lane"]["endLaneIndex"].as<int>();
      my_lap_nr = data[i]["piecePosition"]["lap"].as<int>();
      my_in_piece_distance = data[i]["piecePosition"]["inPieceDistance"].as<double>();

      double my_car_angle_old = my_car_angle;
      my_car_angle = fabs(data[i]["angle"].as<double>());

      double track_straightness = track_is_straight();
      double track_past_straightness = track_was_straight();
      if(!next_piece_is_curve())
        next_curve();

      if(next_piece_has_switch() && next_curve_angle < 181.0)
      {
        //check which lane is most clear and selet that
        //otherwise select the lane that is the inner lane in the next turn
        if(next_curve_angle < 0.0)
        {
          next_curve_angle = 0.0;
          std::cout << "switched Left" << std::endl;
          my_inner_lane = true;
          return { make_lane_switch("Left") };
        }
        else if(next_curve_angle > 0.0)
        {
          next_curve_angle = 0.0;
          std::cout << "switched Right" << std::endl;
          my_inner_lane = true;
          return { make_lane_switch("Right") };
        }
      }
//*/
      //throttle controls
      if((track_straightness < track_past_straightness/3.0) && track_straightness > 0 && !my_first_corner)
      {
          my_car_throttle = 0.1;
          //figure out if curve is inner or outer and compansate proportionally
          std::cout << "curve coming up! throttle " << my_car_throttle <<  " track straightness " <<
          track_straightness << " past straightness " << track_past_straightness << std::endl;
      }
      else if(track_straightness > 120)
      {
          my_car_throttle = max_throttle;
          std::cout << "track is straight! " << my_car_throttle << std::endl;
          //use turbo if the track is really straight
          if(my_turbo_boost && track_straightness > 500)
          {
            std::cout << "TURBOING!" << std::endl;
            my_turbo_boost = false;
            //return { make_turbo("derp!") };
          }
      }
      else if(my_car_angle > (my_car_angle_old + angle_threshold))
      {
        my_first_corner = false;
        /*std::cout << "angle is " << my_car_angle;
        std::cout << "old angle is " << my_car_angle_old << std::endl;
        std::cout << "throttle " << my_car_throttle <<  " track straightness " <<
          track_straightness << " past straightness " << track_past_straightness << std::endl;
       */ 
        if(my_car_throttle - 0.4 < 0.1)
        {
          my_car_throttle = 0.1;
        }
        else 
        {
          my_car_throttle -= 0.4;
          std::cout << "decreasing speed" << my_car_throttle << std::endl;
        }
      }
      //if limited the speed since this dumb solution can't handle the current 
      //track
      else if(my_car_angle < (my_car_angle_old + angle_threshold))
      {
        /*
        std::cout << "angle is " << my_car_angle << std::endl;
        std::cout << "old angle is " << my_car_angle_old << std::endl;
        std::cout << "throttle " << my_car_throttle <<  " track straightness " <<
          track_straightness << " past straightness " << track_past_straightness << std::endl;
          */
        if(my_car_throttle + 0.2 > curve_throttle)
          my_car_throttle = curve_throttle;
        else 
        {
          my_car_throttle += 0.2;
          std::cout << "increasing speed" << my_car_throttle << std::endl;
        }
      }

    }
  }
  return { make_throttle(my_car_throttle) };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
  std::cout << "Someone crashed" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
  std::cout << "Race ended" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_turbo(const jsoncons::json& data)
{
/*
{"msgType": "turboAvailable", "data": {
  "turboDurationMilliseconds": 500.0,
  "turboDurationTicks": 30,
  "turboFactor": 3.0
}}
*/

  my_turbo_boost = true;
  //turbo unstable, need to parse turbo message and calculate if we're likely to crash if we use it
  return { make_ping() };
 // std::cout << "TURBOING!" << std::endl;
  //return { make_turbo("derp!") };
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data)
{
  my_car_id = data["name"].as<std::string>();
  my_car_color = data["color"].as<std::string>();
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
  track_pieces = data["race"]["track"]["pieces"].as<std::vector<std::string>>();
  for(int i=0; i<track_pieces.size(); i++)
    std::cout << "pieces " << track_pieces[i];
  std::cout << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_spawn(const jsoncons::json& data)
{
  std::cout << "in on spawn" << std::endl;
  return { make_ping() };
  //return { set_throttle(max_throttle) };
}

double game_logic::track_was_straight()
{
  double length_accumulator = 0.0;
  for(int i = my_piece_index; ; i = (i-1))
  {
    if(my_lap_nr < 1 && i < 0)
      break;
    if(i < 0)
      i = track_pieces.size()-1;
    //std::cout << track_pieces[i] << " compare equals " <<
     // track_pieces[i].compare(1,8,"\"length\"",0,8) << std::endl;
    //parse wether or not it is a straight piece
    if(track_pieces[i].compare(1,8,"\"length\"",0,8) == 0)
    {
      std::string track_piece = track_pieces[i];
      std::string derp = track_piece.substr(10,track_piece.size()-10-1);
      double distance = atof(track_piece.substr(10,track_piece.size()-10-1).c_str());
      if(i == my_piece_index)
      {
        distance = my_in_piece_distance;
      }
      //add its length to length_accumulator
      length_accumulator += distance;
      //std::cout << "size of derp " << derp.size() << " " << derp << std::endl;
      //std::cout << "double value of doublederp " << distance << std::endl;
      //std::cout << "acc " << length_accumulator << std::endl;
    }
    //break if the piece is a curve
    else
      break;
  }
  if(length_accumulator == 0.0)
    return -1;
  else
    return length_accumulator;
}

double game_logic::track_is_straight()
{
  double length_accumulator = 0.0;
  for(int i = my_piece_index; ; i = (i+1) % track_pieces.size())
  {
    //std::cout << track_pieces[i] << " compare equals " <<
     // track_pieces[i].compare(1,8,"\"length\"",0,8) << std::endl;
    //parse wether or not it is a straight piece
    //std::cout << "i " << i << std::endl;
    std::string track_piece = track_pieces[i];
    if(track_piece.compare(1,8,"\"length\"",0,8) == 0)
    {
      std::string derp = track_piece.substr(10,track_piece.size()-10-1);
      double distance = atof(track_piece.substr(10,track_piece.size()-10-1).c_str());
      if(i == my_piece_index)
      {
        distance -= my_in_piece_distance;
      }
      //add its length to length_accumulator
      length_accumulator += distance;
      //std::cout << "size of derp " << derp.size() << " " << derp << std::endl;
      //std::cout << "double value of doublederp " << distance << std::endl;
      //std::cout << "acc " << length_accumulator << std::endl;
    }
    //break if the piece is a curve and report angle value
    else 
    {
      /*if(true)
      //if(next_curve_angle < 182.0)
      {
        //update next inner lane index
        //{"angle":45.0,"radius":100}
        size_t pos = track_piece.find(":");
        size_t tick_pos = track_piece.find(",");
        if(pos != std::string::npos && tick_pos != std::string::npos)
        {
          next_curve_angle = atof(track_piece.substr(pos+1,tick_pos-pos-1).c_str());
          std::cout << "next angle " << next_curve_angle << std::endl;
        }
        else
          std::cout << "Error in track angle parsing" << track_piece << std::endl;
      }*/
      break;
    }
  }
  if(length_accumulator == 0.0)
  {
    return -1;
  }
  else
    return length_accumulator;
}

void game_logic::next_curve()
{
  double angle_accumulator = 0.0;
  bool first_curve = true;
  bool last_curve = false;
  for(int i = my_piece_index; ; i = (i+1) % track_pieces.size())
  {
    std::string track_piece = track_pieces[i];
    //skip through current curve, if any
    size_t poss = track_piece.find("angle");
    if(poss != std::string::npos)
    {
      if(first_curve)
        continue;
    }
    first_curve = false;
    //skip through next straight, if any
    if(poss == std::string::npos)
    {
      if(last_curve)
      {
        next_curve_angle = angle_accumulator;
        break;
      }
      continue;
    }
    last_curve = true;

    size_t pos = track_piece.find(":");
    size_t tick_pos = track_piece.find(",");
    if(pos != std::string::npos && tick_pos != std::string::npos)
    {
      angle_accumulator += atof(track_piece.substr(pos+1,tick_pos-pos-1).c_str());
      std::cout << "next angle " << next_curve_angle << std::endl;
    }
    else
      std::cout << "Error in track angle parsing" << track_piece << std::endl;
  }
}

bool game_logic::next_piece_has_switch()
{
  std::string track_piece = track_pieces[(my_piece_index+1) % track_pieces.size()];
  size_t pos = track_piece.find("switch");
  if(pos != std::string::npos)
  {
    return true;
  }
  return false;
}
bool game_logic::next_piece_is_curve()
{
  std::string track_piece = track_pieces[(my_piece_index+1) % track_pieces.size()];
  size_t pos = track_piece.find("angle");
  if(pos != std::string::npos)
  {
    return true;
  }
  return false;
}
