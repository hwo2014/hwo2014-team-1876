#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <jsoncons/json.hpp>

class game_logic
{
public:
  typedef std::vector<jsoncons::json> msg_vector;

  game_logic();
  msg_vector react(const jsoncons::json& msg);

private:
  typedef std::function<msg_vector(game_logic*, const jsoncons::json&)> action_fun;
  const std::map<std::string, action_fun> action_map;

  std::string my_car_id;
  std::string my_car_color;
  double my_car_throttle;
  double my_car_angle;
  double my_in_piece_distance;
  double next_curve_angle;
  int my_lap_nr;
  int my_piece_index;
  int my_start_lane_index;
  int my_end_lane_index;
  int my_next_inner_lane_index;
  std::vector<std::string> track_pieces;
  bool my_first_corner;
  bool my_turbo_boost;
  bool my_inner_lane;

  const double angle_threshold = 1.0;
  //maybe have one max_throttle for inner curves and one for outer?
  const double max_throttle = 1.0;
  const double curve_throttle = 0.8;

  msg_vector on_join(const jsoncons::json& data);
  msg_vector on_game_start(const jsoncons::json& data);
  msg_vector on_car_positions(const jsoncons::json& data);
  msg_vector on_crash(const jsoncons::json& data);
  msg_vector on_game_end(const jsoncons::json& data);
  msg_vector on_error(const jsoncons::json& data);
  msg_vector on_turbo(const jsoncons::json& data);
  msg_vector on_your_car(const jsoncons::json& data);
  msg_vector on_game_init(const jsoncons::json& data);
  msg_vector on_spawn(const jsoncons::json& data);

  double track_is_straight();
  double track_was_straight();
  void next_curve();
  bool next_piece_has_switch();
  bool next_piece_is_curve();
};

#endif
